// Copyright (c) 2021, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::path::Path;

fn main() {
    #[cfg(windows)]
    let gl_report_path = dirs::home_dir().expect("Couldn't find home directory").join(".gitlab-report.exe");
    #[cfg(unix)]
    let gl_report_path = dirs::home_dir().expect("Couldn't find home directory").join(".gitlab-report");
    let var = std::env::var("PROFILE").unwrap();
    #[cfg(windows)]
    let str = if var == "debug" {
        "../target/debug/cargo-gitlab.exe"
    } else {
        "../target/release/cargo-gitlab.exe"
    };
    #[cfg(unix)]
    let str = if var == "debug" {
        "../target/debug/cargo-gitlab"
    } else {
        "../target/release/cargo-gitlab"
    };
    if !Path::new(str).exists() || !gl_report_path.exists() {
        panic!("The compiled binaries are not available please build cargo-build first");
    }
    println!("cargo:rustc-env=GL_REPORT_PATH={}", gl_report_path.display());
    println!("cargo:rustc-env=GL_BUILD_PATH={}", str);
}