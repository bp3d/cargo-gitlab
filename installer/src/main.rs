// Copyright (c) 2021, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::fs::File;
use std::io::{BufWriter, Write};
use std::path::{Path, PathBuf};

const READ_LEN: usize = 4096;

const EXEC_FILE_MODE: u32 = 0o777;

#[cfg(windows)]
const REPORT_PROG_NAME: &str = ".gitlab-report.exe";
#[cfg(unix)]
const REPORT_PROG_NAME: &str = ".gitlab-report";

#[cfg(windows)]
const PROG_NAME: &str = "cargo-gitlab.exe";
#[cfg(unix)]
const PROG_NAME: &str = "cargo-gitlab";

const MAIN_PROG: &[u8] = include_bytes!(concat!("../", env!("GL_BUILD_PATH")));
const REPORT_PROG: &[u8] = include_bytes!(env!("GL_REPORT_PATH"));

const ERR_HOME: &str = "Couldn't find home directory";
const ERR_INSTALL: &str = "Couldn't install cargo-gitlab";
const ERR_INSTALL_REPORT: &str = "Couldn't install gitlab-report";

fn unpack_file(file: &[u8], err: &str, mut out_file: BufWriter<File>)
{
    let mut cursor = 0;
    while cursor < file.len() {
        let end = std::cmp::min(cursor + READ_LEN, file.len());
        out_file.write_all(&file[cursor..end]).expect(err);
        cursor += READ_LEN;
    }
}

fn create_exec_file(path: &Path) -> std::io::Result<File>
{
    #[cfg(unix)]
    {
        use std::fs::OpenOptions;
        use std::os::unix::fs::OpenOptionsExt;
        OpenOptions::new().write(true).create(true).truncate(true).mode(EXEC_FILE_MODE).open(path)
    }
    #[cfg(windows)]
    {
        File::create(path)
    }
}

fn unpack_gitlab_report()
{
    println!("Installing gitlab-report...");
    let out_path = dirs::home_dir().expect(ERR_HOME).join(REPORT_PROG_NAME);
    let out_file = BufWriter::new(create_exec_file(&out_path).expect(ERR_INSTALL_REPORT));
    unpack_file(REPORT_PROG, ERR_INSTALL_REPORT, out_file);
}

fn get_cargo_bin_home() -> PathBuf
{
    if let Some(path) = std::env::var_os("CARGO_HOME") {
        Path::new(&path).join("bin")
    } else {
        dirs::home_dir().expect(ERR_HOME).join(".cargo/bin")
    }
}

fn unpack_main_prog()
{
    println!("Installing cargo-gitlab...");
    let bin_path = get_cargo_bin_home();
    if !bin_path.exists() {
        std::fs::create_dir_all(&bin_path).expect("Couldn't create cargo bin directory");
    }
    let out_path = bin_path.join(PROG_NAME);
    let out_file = BufWriter::new(create_exec_file(&out_path).expect(ERR_INSTALL));
    unpack_file(MAIN_PROG, ERR_INSTALL, out_file);
}

fn main()
{
    unpack_gitlab_report();
    unpack_main_prog();
    println!("cargo-gitlab is now installed");
}
