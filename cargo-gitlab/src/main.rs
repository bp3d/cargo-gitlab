// Copyright (c) 2021, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::io::{BufReader, BufWriter, Read, Write};
use std::path::PathBuf;
use std::process::Stdio;
use crate::clippy_to_html::run_cargo_metadata;

mod clippy_to_html;

const READ_BUF_SIZE: usize = 4096;

fn gl_report_path() -> PathBuf {
    #[cfg(windows)]
    return dirs::home_dir()
        .expect("Couldn't find a user home directory")
        .join(".gitlab-report.exe");
    #[cfg(unix)]
    return dirs::home_dir()
        .expect("Couldn't find a user home directory")
        .join(".gitlab-report");
}

fn cargo_report_pipeline(cargo_args: &[&str], report_args: &[&str], mut buffer: Option<&mut Vec<u8>>) -> i32 {
    let mut cargo = std::process::Command::new("cargo")
        .args(cargo_args)
        .stdout(Stdio::piped())
        .spawn()
        .expect("Failed to start cargo");
    let mut reporter = std::process::Command::new(gl_report_path())
        .args(report_args)
        //.stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .spawn()
        .expect("Failed to start reporter");
    let mut buf: [u8; READ_BUF_SIZE] = [0; READ_BUF_SIZE];
    {
        let mut stdin = reporter.stdin.take().map(BufWriter::new).unwrap();
        let mut stdout = cargo.stdout.take().map(BufReader::new).unwrap();
        while cargo.try_wait().expect("Failed to poll cargo").is_none() {
            let len = stdout.read(&mut buf).expect("Failed to read cargo output");
            if let Some(content) = &mut buffer {
                content.write_all(&buf[..len]).expect("Failed to write into buffer");
            }
            stdin
                .write_all(&buf[..len])
                .expect("Failed to send cargo output to gitlab-report");
        }
    }
    let status = cargo.wait().expect("Failed to read cargo exit status");
    status.code().unwrap_or(0)
}

fn cargo_run_clippy() -> i32
{
    let mut buffer = Vec::new();
    cargo_report_pipeline(
        &["clippy", "--all-features", "--message-format=json"],
        &["-p", "clippy", "-o", "gl-code-quality-report.json"],
        Some(&mut buffer)
    );
    run_cargo_metadata(buffer);
    0
}

fn main() {
    /*
     * Available subcommands:
     *  - audit
     *  - test
     *  - bench
     *  - clippy
     *  - geiger
     */
    let sub_cmd = std::env::args()
        .nth(2)
        .expect("Please specify a subcommand");

    let res = match &*sub_cmd {
        "audit" => cargo_report_pipeline(
            &["audit", "--color=always", "--json"],
            &["-p", "audit", "-f", "gl-sast", "-o", "gl-sast-report.json"],
            None
        ),
        "test" => cargo_report_pipeline(
            &[
                "test",
                "--all-features",
                "--no-fail-fast",
                "--",
                "-Z",
                "unstable-options",
                "--format",
                "json",
            ],
            &["-p", "test", "-o", "report.xml"],
            None
        ),
        "bench" => cargo_report_pipeline(
            &[
                "bench",
                "--all-features",
                "--",
                "-Z",
                "unstable-options",
                "--format",
                "json",
            ],
            &["-p", "bench", "-o", "metrics.txt"],
            None
        ),
        "clippy" => cargo_run_clippy(),
        _ => {
            eprintln!("Invalid subcommand, allowed values are: audit, test, bench, clippy");
            1
        }
    };
    std::process::exit(res);
}
