// Copyright (c) 2021, BlockProject 3D
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright notice,
//       this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright notice,
//       this list of conditions and the following disclaimer in the documentation
//       and/or other materials provided with the distribution.
//     * Neither the name of BlockProject 3D nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use std::fs::File;
use std::io::{BufRead, BufWriter, Read, Write};
use cargo_metadata::{CompilerMessage, Message};
use cargo_metadata::diagnostic::DiagnosticLevel;
use maud::{html, Markup, DOCTYPE};

//This is because cargo_metadata is defective and doesn't allow flexible read
pub struct Hack<'a>(&'a [u8]);

impl<'a> Read for Hack<'a> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        self.0.read(buf)
    }
}

impl<'a> BufRead for Hack<'a>
{
    fn fill_buf(&mut self) -> std::io::Result<&[u8]> {
        Ok(self.0)
    }

    fn consume(&mut self, amt: usize) {
        self.0 = &self.0[amt..];
    }
}

fn html_msg(msg: CompilerMessage) -> Markup
{
    let name = match msg.message.level {
        DiagnosticLevel::Ice => "ICE",
        DiagnosticLevel::Error => "Error",
        DiagnosticLevel::Warning => "Warning",
        DiagnosticLevel::FailureNote => "FailureNote",
        DiagnosticLevel::Note => "Note",
        DiagnosticLevel::Help => "Help",
        _ => "Unknown"
    };
    let color = match msg.message.level {
        DiagnosticLevel::Ice => "darkred",
        DiagnosticLevel::Error => "red",
        DiagnosticLevel::Warning => "yellow",
        DiagnosticLevel::FailureNote => "orange",
        DiagnosticLevel::Note => "blue",
        _ => "black"
    };
    let code = msg.message.code.unwrap().code;
    html! {
        div .msg {
            div .msg-title .{ "msg-title-" (color) } { (name) ": " (msg.message.message) }
            div .msg-body-outline {
                div .msg-body { (msg.message.rendered.unwrap_or_default()) }
            }
            div .msg-code { (code) }
        }
    }
}

fn html(msgs: Vec<Markup>) -> Markup
{
    html! {
        (DOCTYPE)
        head {
            meta charset="utf-8";
            title { "Clippy HTML report" }
            style {
                r#"
                    body {
                        width: 80%;
                        margin-left: auto;
                        margin-right: auto;
                        font-family: sans-serif;
                        font-size: 12pt
                    }

                    .msg {
                        background-color: rgb(240, 240, 240);
                        padding: 16px;
                        border-radius: 8px;
                        margin: 8px;
                    }

                    .msg-title {
                        font-weight: 900;
                        background-color: white;
                        border-radius: 4px;
                        padding: 4px;
                        margin-bottom: 16px;
                    }

                    .msg-code {
                        background-color: white;
                        border-radius: 4px;
                        padding: 4px;
                        margin-top: 16px;
                        font-style: italic;
                    }

                    .msg-body-outline {
                        padding: 8px;
                        border-radius: 4px;
                        border: 0.1em solid black;
                    }

                    .msg-body {
                        white-space: pre;
                        overflow: auto;
                    }

                    .msg-title-darkred {
                        color: darkred;
                    }

                    .msg-title-red {
                        color: red;
                    }

                    .msg-title-yellow {
                        color: orange;
                    }

                    .msg-title-orange {
                        color: orangered;
                    }

                    .msg-title-blue {
                        color: blue;
                    }

                    .msg-title-black {
                        color: black;
                    }
                "#
            }
        }
        body {
            @for msg in msgs {
                (msg)
            }
        }
    }
}

pub fn run_cargo_metadata(data: Vec<u8>)
{
    let mut msgs = Vec::new();
    let hack = Hack(&data);
    for msg in Message::parse_stream(hack) {
        if let Message::CompilerMessage(msg) = msg.expect("Couldn't parse cargo output") {
            if msg.message.code.is_some() {
                msgs.push(html_msg(msg));
            }
        }
    }
    let html = html(msgs).into_string();
    let mut f = BufWriter::new(File::create("./gl-code-quality-report.html").expect("Failed to create clippy HTML report"));
    f.write_all(html.as_bytes()).expect("Failed to write clippy HTML report");
}
