use std::path::PathBuf;

const DL_FAIL: &str = "Failed to download rust-gitlab-ci dependency";
const BUILD_FAIL: &str = "Failed to build gitlab-report dependency";

fn main()
{
    //Unfortunately 'cargo:rerun-if-changed=' is now broken so let's emulate it's behavior
    #[cfg(windows)]
    let final_path = dirs::home_dir().expect("Couldn't find a user home directory").join(".gitlab-report.exe");
    #[cfg(unix)]
    let final_path = dirs::home_dir().expect("Couldn't find a user home directory").join(".gitlab-report");
    if final_path.exists() {
        return;
    }
    //End

    let out_dir = PathBuf::from(std::env::var("OUT_DIR").unwrap());
    let git_dir = out_dir.join("rust-gitlab-ci");
    if git_dir.exists() {
        std::fs::remove_dir_all(&git_dir).expect("Failed to cleanup build cache");
    }
    let res = std::process::Command::new("git")
        .arg("clone")
        .arg("https://gitlab.com/TobiP64/rust-gitlab-ci.git")
        .current_dir(&out_dir)
        .status().expect(DL_FAIL);
    if !res.success() {
        panic!("{}", DL_FAIL);
    }
    let res = std::process::Command::new("cargo")
        .arg("build")
        .arg("--release")
        .current_dir(out_dir.join("rust-gitlab-ci/gitlab-report"))
        .status().expect(BUILD_FAIL);
    if !res.success() {
        panic!("{}", BUILD_FAIL);
    }
    #[cfg(windows)]
    let bin_path = git_dir.join("target/release/gitlab-report.exe");
    #[cfg(unix)]
    let bin_path = git_dir.join("target/release/gitlab-report");
    if !bin_path.exists() {
        panic!("{}", BUILD_FAIL);
    }
    std::fs::copy(bin_path, final_path).expect("Couldn't copy the gitlab-report tool to user home");
}